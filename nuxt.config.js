require('dotenv').config()

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Storm',
    titleTemplate: '%s · Storm',
    htmlAttrs: {
      lang: 'en-US',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      {
        hid: 'description',
        name: 'description',
        content: 'Weather application',
      },
      { property: 'og:title', content: 'Storm' },
      { property: 'og:description', content: 'Weather application' },
      { property: 'og:image', content: '/icon.png' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  loading: {
    color: '#4a81bd',
    height: '5px',
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/icons-loader', ssr: false },
    { src: '~/plugins/vue-tailwind-screens', mode: 'client' },
    { src: '~/plugins/vue-helper' },
    { src: '~/plugins/vue-scrollto' },
    { src: '~/plugins/vue-luxon' },
    { src: '~/plugins/v-tooltip' },
    { src: '~/plugins/vue-lazy-youtube-video' },
    { src: '~/plugins/vue-youtube' },
    { src: '~/plugins/vue-awesome-swiper' },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://github.com/nuxt-community/global-components
    '@nuxtjs/global-components',
    // Doc: https://github.com/nuxt-community/router-module
    '@nuxtjs/router',
  ],

  tailwindcss: {
    cssPath: '~/assets/css/tailwind.css',
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://github.com/nuxt-community/svg-module
    '@nuxtjs/svg',
    // https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // https://github.com/rigor789/vue-scrollto
    ['vue-scrollto/nuxt', { duration: 300 }],
    // https://http.nuxtjs.org/
    '@nuxt/http',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // https://github.com/nuxt-community/global-components
  globalComponents: {},

  // https://http.nuxtjs.org/
  http: {
    // proxyHeaders: false
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
