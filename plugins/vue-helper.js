import Vue from 'vue'

// slugify text
const slugify = (text) => {
  if (text) {
    return text
      .toString()
      .toLowerCase()
      .replace(/["']/i, '-')
      .replace(/\s+/g, '-')
      .normalize('NFD')
      .replace(/[\u0300-\u036F]/g, '')
  }
}

Vue.prototype.$slugify = slugify

const shuffle = (a) => {
  let j, x, i
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
  }
  return a
}

Vue.prototype.$shuffle = shuffle

const timestampToDatetime = (unixTimestamp) => {
  const milliseconds = unixTimestamp * 1000
  const dateObject = new Date(milliseconds)
  const date = dateObject.toLocaleString() // 2019-12-9 10:30:15
  const time = dateObject.toLocaleTimeString()
  // dateObject.toLocaleString('en-US', { weekday: 'long' }) // Monday
  // dateObject.toLocaleString('en-US', { month: 'long' }) // December
  // dateObject.toLocaleString('en-US', { day: 'numeric' }) // 9
  // dateObject.toLocaleString('en-US', { year: 'numeric' }) // 2019
  // dateObject.toLocaleString('en-US', { hour: 'numeric' }) // 10 AM
  // dateObject.toLocaleString('en-US', { minute: 'numeric' }) // 30
  // dateObject.toLocaleString('en-US', { second: 'numeric' }) // 15
  // dateObject.toLocaleString('en-US', { timeZoneName: 'short' }) // 12/9/2019, 10:30:15 AM CST

  return { date, time }
}

Vue.prototype.$timestampToDatetime = timestampToDatetime

const getDateTime = (dateTime) => {
  const milliseconds = dateTime * 1000
  dateTime = new Date(milliseconds)
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }
  dateTime = new Date(dateTime)
  const userLang = navigator.language || navigator.userLanguage
  const date = dateTime.toLocaleDateString(userLang, options)
  const time = dateTime.toLocaleTimeString()

  return {
    time,
    date,
  }
}

Vue.prototype.$getDateTime = getDateTime

const differenceBetweenTwoDate = (d1, d2) => {
  const diffTime = Math.abs(d2 - d1)
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))
  // console.log(diffTime + ' milliseconds')
  // console.log(diffDays + ' days')

  return {
    time: diffTime / 1000,
    days: diffDays,
  }
}

Vue.prototype.$differenceBetweenTwoDate = differenceBetweenTwoDate
