import Vue from 'vue'
// import VueAwesomeSwiper from 'vue-awesome-swiper'

import {
  Swiper as SwiperClass,
  Pagination,
  Mousewheel,
  Autoplay,
} from 'swiper/core'
import getAwesomeSwiper from 'vue-awesome-swiper/dist/exporter'
// import style (>= Swiper 6.x)
import 'swiper/swiper-bundle.css'

// Swiper modules
SwiperClass.use([Pagination, Mousewheel, Autoplay])
Vue.use(getAwesomeSwiper(SwiperClass))
