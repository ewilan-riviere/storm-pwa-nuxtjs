# Storm

A weather app which use [**openweathermap.org**](https://openweathermap.org) API to display current day weather data and for a week. Built with NuxtJS and available in PWA!

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
