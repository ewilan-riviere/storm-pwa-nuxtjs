export const state = () => ({
  sidebarOpened: false,
  layerVisible: false,
  routes: [
    // { label: 'Articles', routeName: 'posts' },
    // { label: 'Nos clients', routeName: 'clients' },
    // { label: 'Notre équipe', routeName: 'team' },
    // { label: 'À propos', routeName: 'about' },
  ],
  lastDate: null,
  weather: {},
})

export const mutations = {
  toggleSidebarOpened(state, data) {
    state.sidebarOpened = !state.sidebarOpened
  },
  setSidebarOpened(state, data) {
    state.sidebarOpened = data
  },
  toggleLayerVisible(state, data) {
    state.layerVisible = !state.layerVisible
  },
  setLayerVisible(state, data) {
    state.layerVisible = data
  },
  setLastDate(state, data) {
    state.lastDate = data
  },
  setWeather(state, data) {
    state.weather = data
  },
}
